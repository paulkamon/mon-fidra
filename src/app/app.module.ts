import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PaymentsPage } from '../pages/payments/payments';
import { ActiveLoanListPage } from '../pages/active-loan-list/active-loan-list';
import { AccountListPage } from '../pages/account-list/account-list';
import { ManagePayeePage } from '../pages/manage-payee/manage-payee';
import { FdListPage } from '../pages/fd-list/fd-list';
import { SupportPage } from '../pages/support/support';
import { OtherTransferPage } from '../pages/other-transfer/other-transfer';
import { LegalTermsPage } from '../pages/legal-terms/legal-terms';
import { OtpPage } from '../pages/otp/otp';
import { FdCertificatePage } from '../pages/fd-certificate/fd-certificate';
import { WelComePage } from '../pages/wel-come/wel-come';
import { EnrollFingurePage } from '../pages/enroll-fingure/enroll-fingure';
import { CreateMpinPage } from '../pages/create-mpin/create-mpin';
import { PayeeListPage } from '../pages/payee-list/payee-list';
import { AddPayeePage } from '../pages/add-payee/add-payee';
import { InvestmentPage } from '../pages/investment/investment';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    // AccountListPage,
    // PaymentsPage,
    // ActiveLoanListPage,
    // ManagePayeePage,
    // FdListPage,
    // SupportPage,
    // OtherTransferPage,
    // LegalTermsPage,
    // OtpPage,
    // FdCertificatePage,
    // WelComePage,
    // EnrollFingurePage,
    // CreateMpinPage,
    // PayeeListPage,
    // AddPayeePage,
    // InvestmentPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    // AccountListPage,
    // PaymentsPage,
    // ActiveLoanListPage,
    // ManagePayeePage,
    // FdListPage,
    // SupportPage,
    // OtherTransferPage,
    // LegalTermsPage,
    // OtpPage,
    // FdCertificatePage,
    // WelComePage,
    // EnrollFingurePage,
    // CreateMpinPage,
    // PayeeListPage,
    // AddPayeePage,
    // InvestmentPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FingerprintAIO,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
