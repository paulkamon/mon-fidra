import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
// import { EnrollFingurePage } from '../enroll-fingure/enroll-fingure';
// import { CreateMpinPage } from '../create-mpin/create-mpin';

@IonicPage()
@Component({
  selector: 'page-wel-come',
  templateUrl: 'wel-come.html',
})
export class WelComePage {
  sign:string = "signin";
  isAndroid:boolean = false;
  loading;
  Mobile;Password;pushData;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelComePage');
  }
  Login()
  {
    this.presentLoadingDefault();
    this.navCtrl.setRoot('CreateMpinPage');
    this.loading.dismiss();
   
  }
  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
      <div class="custom-spinner-container">
      <img class="loading" width="100px" height="100px" src="assets/imgs/Loader.gif" />
      </div>`,
      duration: 5000
    });
    this.loading.present();
  }
  ForgetPassword()
  {

   /*  this.navCtrl.setRoot(ForgotPasswordPage); */
  }
}
