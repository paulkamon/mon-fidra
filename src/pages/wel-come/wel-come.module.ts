import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WelComePage } from './wel-come';

@NgModule({
  declarations: [
    WelComePage,
  ],
  imports: [
    IonicPageModule.forChild(WelComePage),
  ],
})
export class WelComePageModule {}
