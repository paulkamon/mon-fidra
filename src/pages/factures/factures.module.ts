import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FacturesPage } from './factures';

@NgModule({
  declarations: [
    FacturesPage,
  ],
  imports: [
    IonicPageModule.forChild(FacturesPage),
  ],
})
export class FacturesPageModule {}
