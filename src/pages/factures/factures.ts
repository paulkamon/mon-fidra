import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App } from 'ionic-angular';

/**
 * Generated class for the FacturesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-factures',
  templateUrl: 'factures.html',
})
export class FacturesPage {

  constructor(public app: App,public navCtrl: NavController, public navParams: NavParams, private alertCtrl:AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FacturesPage');
  }

  openModal(){
    let alert = this.alertCtrl.create({
      title: 'Déconnexion',
      subTitle: 'Voulez vous vraiment vous déconnecter?',
     // buttons: ['OK']
      buttons: [ {
        cssClass: 'alertCustomCss',
        text: 'NON',
        handler: () => {
          // this.navCtrl.setRoot('RegisterPage');
          console.log('Disagree clicked');
        }
      },
      {
        cssClass: 'alertCustomCss',
        text: 'OUI',
        handler: () => {
          this.app.getRootNav().setRoot('WelComePage');
          console.log('Continuer clicked');
        }
      }]

    });
    alert.present();
  }


  Bankaccountlist()
  {
    this.navCtrl.push('AccountListPage').then(()=> {
      this.navCtrl.remove(this.navCtrl.getPrevious().index);
    });
  }
  demandes()
  {
    this.navCtrl.push('DemandesPage').then(()=> {
      this.navCtrl.remove(this.navCtrl.getPrevious().index);
    });
  }
  produits()
  {
    this.navCtrl.push('ProduitsPage').then(()=> {
      this.navCtrl.remove(this.navCtrl.getPrevious().index);
    });
  }
  // factures()
  // {
  //   this.navCtrl.push('FacturesPage');
  // }

}
