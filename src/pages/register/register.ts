import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController,Platform } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  sign:string = "signin";
  isAndroid:boolean = false;
  loading;
  Mobile;Password;pushData;
  constructor(public navCtrl: NavController, private platform:Platform, private faio: FingerprintAIO, public navParams: NavParams, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelComePage');
  }
  Login()
  {
    this.presentLoadingDefault();
    this.navCtrl.setRoot(TabsPage);
    this.loading.dismiss();
   
  }
  async showAlert() {
    try {
     await this.platform.ready();
     const available = await this.faio.isAvailable();
     console.log(available);
     if (available === "OK") {
      this.faio.show({
        clientId: 'Fingerprint-Demo',
        clientSecret: 'password', //Only necessary for Android
        disableBackup:true,  //Only for Android(optional)
        localizedFallbackTitle: 'Use Pin', //Only for iOS
        localizedReason: 'Please authenticate' //Only for iOS
    })
    .then((result: any) => {
      console.log(result);
      this.navCtrl.setRoot(TabsPage);
    })
    .catch((error: any) => console.log(error));

     } else {
       alert('Non disponible')
     }
     
    } catch (error) {
      console.error('try',error);
      alert(error.message)
    }
    
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
      <div class="custom-spinner-container">
      <img class="loading" width="100px" height="100px" src="assets/imgs/Loader.gif" />
      </div>`,
      duration: 5000
    });
    this.loading.present();
  }
  ForgetPassword()
  {

   /*  this.navCtrl.setRoot(ForgotPasswordPage); */
  }
}
