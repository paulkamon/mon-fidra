import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PayeeListPage } from '../payee-list/payee-list';
import { AddPayeePage } from '../add-payee/add-payee';



@IonicPage()
@Component({
  selector: 'page-manage-payee',
  templateUrl: 'manage-payee.html',
})
export class ManagePayeePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ManagePayeePage');
  }
  PayeeList()
  {
    this.navCtrl.push(PayeeListPage);
  }

  AddPayee()
  {
    this.navCtrl.push(AddPayeePage);
  }
}
