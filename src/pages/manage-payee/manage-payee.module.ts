import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManagePayeePage } from './manage-payee';

@NgModule({
  declarations: [
    ManagePayeePage,
  ],
  imports: [
    IonicPageModule.forChild(ManagePayeePage),
  ],
})
export class ManagePayeePageModule {}
