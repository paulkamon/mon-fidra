import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App } from 'ionic-angular';
// import { OtpPage } from '../otp/otp';


@IonicPage()
@Component({
  selector: 'page-payments',
  templateUrl: 'payments.html',
})
export class PaymentsPage {

  bankaccount;ActiveBenificaryData;
  constructor(public app: App,public navCtrl: NavController, private alertCtrl:AlertController,public navParams: NavParams) {
    this.bankaccount = [
      {"Srno":1,"AccountNo":"LT100001278797","Balance": "$ 1500"}
      
  ];

  this.ActiveBenificaryData = [
    {"BeneficiaryId":1,"BeneficiaryName":"Carte"},
    {"BeneficiaryId":2,"BeneficiaryName":"Mobile Money"},
    {"BeneficiaryId":2,"BeneficiaryName":"FIDRA"}
    
];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentsPage');
  }

openModal(){
    let alert = this.alertCtrl.create({
      title: 'Déconnexion',
      subTitle: 'Voulez vous vraiment vous déconnecter?',
     // buttons: ['OK']
      buttons: [ {
        cssClass: 'alertCustomCss',
        text: 'NON',
        handler: () => {
          // this.navCtrl.setRoot('RegisterPage');
          console.log('Disagree clicked');
        }
      },
      {
        cssClass: 'alertCustomCss',
        text: 'OUI',
        handler: () => {
          // this.navCtrl.setRoot('WelComePage');
          this.app.getRootNav().setRoot('WelComePage');
          console.log('Continuer clicked');
        }
      }]

    });
    alert.present();
  }

  OTP()
  {
    this.navCtrl.push('OtpPage');
  }
}