import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  account: string = "profile";
  constructor(public navCtrl: NavController, private alertCtrl:AlertController) {

  }

  openModal(){
    let alert = this.alertCtrl.create({
      title: 'Déconnexion',
      subTitle: 'Vouez vous vraiment vous déconnecter?',
     // buttons: ['OK']
      buttons: [ {
        cssClass: 'alertCustomCss',
        text: 'Annuler',
        handler: () => {
          // this.navCtrl.setRoot('RegisterPage');
          console.log('Disagree clicked');
        }
      },
      {
        cssClass: 'alertCustomCss',
        text: 'Continuer',
        handler: () => {
          this.navCtrl.setRoot('WelComePage');
          console.log('Continuer clicked');
        }
      }]

    });
    alert.present();
  }
}
