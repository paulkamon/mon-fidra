import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FdCertificatePage } from './fd-certificate';

@NgModule({
  declarations: [
    FdCertificatePage,
  ],
  imports: [
    IonicPageModule.forChild(FdCertificatePage),
  ],
})
export class FdCertificatePageModule {}
