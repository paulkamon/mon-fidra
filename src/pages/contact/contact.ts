import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

declare var google;

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  constructor(public navCtrl: NavController, private alertCtrl:AlertController) {

  }
  ionViewDidLoad(){
    this.loadMap();
  }

  openModal(){
    let alert = this.alertCtrl.create({
      title: 'Déconnexion',
      subTitle: 'Vouez vous vraiment vous déconnecter?',
     // buttons: ['OK']
      buttons: [ {
        cssClass: 'alertCustomCss',
        text: 'Annuler',
        handler: () => {
          // this.navCtrl.setRoot('RegisterPage');
          console.log('Disagree clicked');
        }
      },
      {
        cssClass: 'alertCustomCss',
        text: 'Continuer',
        handler: () => {
          this.navCtrl.setRoot('WelComePage');
          console.log('Continuer clicked');
        }
      }]

    });
    alert.present();
  }

  loadMap(){

   /*  this.geolocation.getCurrentPosition().then((position) => {

      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    }, (err) => {
      console.log(err);
    });
 */
  }
}
