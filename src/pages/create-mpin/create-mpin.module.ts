import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateMpinPage } from './create-mpin';

@NgModule({
  declarations: [
    CreateMpinPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateMpinPage),
  ],
})
export class CreateMpinPageModule {}
