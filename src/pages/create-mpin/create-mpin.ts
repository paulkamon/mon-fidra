import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { TabsPage } from '../tabs/tabs';
// import { EnrollFingurePage } from '../enroll-fingure/enroll-fingure';
/**
 * Generated class for the CreateMpinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-mpin',
  templateUrl: 'create-mpin.html',
})
export class CreateMpinPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateMpinPage');
  }
  next(el) {
    el.setFocus()
  }


  dashboard()
  {
    this.navCtrl.setRoot('EnrollFingurePage');
  }
}
