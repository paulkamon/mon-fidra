import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OtherTransferPage } from './other-transfer';

@NgModule({
  declarations: [
    OtherTransferPage,
  ],
  imports: [
    IonicPageModule.forChild(OtherTransferPage),
  ],
})
export class OtherTransferPageModule {}
