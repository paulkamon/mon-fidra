import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-other-transfer',
  templateUrl: 'other-transfer.html',
})
export class OtherTransferPage {
  Currency;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.Currency = [
      {"Srno":1,"Currency":"€ - EURO"},
      {"Srno":2,"Currency":"$ - USD"},
      {"Srno":3,"Currency":"RS - INR"}
  ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtherTransferPage');
  }

}
