import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PayeeListPage } from './payee-list';

@NgModule({
  declarations: [
    PayeeListPage,
  ],
  imports: [
    IonicPageModule.forChild(PayeeListPage),
  ],
})
export class PayeeListPageModule {}
