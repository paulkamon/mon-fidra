import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActiveLoanListPage } from './active-loan-list';

@NgModule({
  declarations: [
    ActiveLoanListPage,
  ],
  imports: [
    IonicPageModule.forChild(ActiveLoanListPage),
  ],
})
export class ActiveLoanListPageModule {}
