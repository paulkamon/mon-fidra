import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FdCertificatePage } from '../fd-certificate/fd-certificate';

/**
 * Generated class for the FdListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fd-list',
  templateUrl: 'fd-list.html',
})
export class FdListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FdListPage');
  }
//////////////////////// FUNCTION TO GET FD DETAILS //////////////////////////
FdCertificate()
{
 this.navCtrl.push(FdCertificatePage);
}
///////////////////////// END FUNCTION TO GET FD DETAILS//////////////////////// 
}
