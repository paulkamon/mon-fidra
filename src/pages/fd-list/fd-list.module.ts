import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FdListPage } from './fd-list';

@NgModule({
  declarations: [
    FdListPage,
  ],
  imports: [
    IonicPageModule.forChild(FdListPage),
  ],
})
export class FdListPageModule {}
