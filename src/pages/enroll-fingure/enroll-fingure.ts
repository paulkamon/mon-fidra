import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , Platform} from 'ionic-angular';
import { AlertController } from 'ionic-angular';
// import { TabsPage } from '../tabs/tabs';
// import { CreateMpinPage } from '../create-mpin/create-mpin';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';


@IonicPage()
@Component({
  selector: 'page-enroll-fingure',
  templateUrl: 'enroll-fingure.html',
})
export class EnrollFingurePage {

  constructor(public navCtrl: NavController, private platform:Platform, private faio: FingerprintAIO,public navParams: NavParams,public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EnrollFingurePage');
  }


  async showAlert() {
    try {
     await this.platform.ready();
     const available = await this.faio.isAvailable();
     console.log(available);
     if (available === "OK") {
      this.faio.show({
        clientId: 'Fingerprint-Demo',
        clientSecret: 'password', //Only necessary for Android
        disableBackup:true,  //Only for Android(optional)
        localizedFallbackTitle: 'Use Pin', //Only for iOS
        localizedReason: 'Please authenticate' //Only for iOS
    })
    .then((result: any) => {
      console.log(result);
      this.navCtrl.setRoot('RegisterPage');
    })
    .catch((error: any) => console.log(error));

     } else {
        let alert = this.alertCtrl.create({
          // title: 'Finger Enrolled!',
          subTitle: 'Non disponible',
         // buttons: ['OK']
          buttons: [ {
            cssClass: 'alertCustomCss',
            text: 'Ok',
            handler: () => {
              console.log('ok clicked');
            }
          },]
    
        });
        alert.present();
     }
     
    } catch (error) {
      console.error('try',error);
      alert(error.message)
    }
    
    
    // let alert = this.alertCtrl.create({
    //   title: 'Finger Enrolled!',
    //   subTitle: 'Your Finger Id Has Been Enrolled Successfully!',
    //  // buttons: ['OK']
    //   buttons: [ {
    //     cssClass: 'alertCustomCss',
    //     text: 'Continue',
    //     handler: () => {
    //       this.navCtrl.setRoot('RegisterPage');
    //       console.log('Disagree clicked');
    //     }
    //   },]

    // });
    // alert.present();

  }


  showConfirm() {
    this.navCtrl.setRoot('RegisterPage');
    // let confirm = this.alertCtrl.create({
    //   title: 'Terms And Conditions?',
    //   message: 'By downloading or using the app, these terms will automatically apply to you – you should make sure therefore that you read them carefully before using the app. We are offering you this app to use for your own personal use without cost, but you should be aware that you cannot send it on to anyone else, and you’re not allowed to copy, or modify the app, any part of the app, or our trademarks in any way. You’re not allowed to attempt to extract the source code of the app, and you also shouldn’t try to translate the app into other languages, or make derivative versions. The app itself, and all the trade marks, copyright, database rights and other intellectual property rights related to it, still belong to easyJet.',
      
      
    //   buttons: [
    //     {
    //       cssClass: 'secondary',
    //       text: 'Disagree',
    //       handler: () => {
    //         console.log('Disagree clicked');
    //       }
    //     },
    //     {
    //       text: 'Agree',
    //       handler: () => {
    //         console.log('Agree clicked');
    //         this.navCtrl.setRoot(TabsPage);
    //       }
    //     }
    //   ],
      
      
    // });
    // confirm.present();
  }

}
