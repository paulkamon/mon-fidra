import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EnrollFingurePage } from './enroll-fingure';

@NgModule({
  declarations: [
    EnrollFingurePage,
  ],
  imports: [
    IonicPageModule.forChild(EnrollFingurePage),
  ],
})
export class EnrollFingurePageModule {}
