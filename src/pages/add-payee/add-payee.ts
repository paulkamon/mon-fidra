import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
/**
 * Generated class for the AddPayeePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-payee',
  templateUrl: 'add-payee.html',
})
export class AddPayeePage {
  Customerinfo;Name;customerid;custbranchid;
  IFSC;AccountNo;
  urlchk;
  ifsc;city;
  BeneficiaryAccountName; BeneficiaryAccountNo; BeneficaryContactNo; BeneficaryEmailId; 
  BranchName; BranchCode; IFSCCode; BranchCity; BankName; Mode; CustomerID; BranchId;
  constructor(public navCtrl: NavController, public navParams: NavParams,  public alertCtrl: AlertController) {
  }


  Askotp() {
    let prompt = this.alertCtrl.create({
      title: 'OTP',
      message: "Enter OTP To Authorise the Transcation.",
      inputs: [
        {
          name: 'title',
          placeholder: 'Title'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirm',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  }
   ///////////////////////// FUNCTION TO VERIFY IFSC CODE //////////////////////////
ifscverify()
{

    
 }
 
///////////////////////// END FUNCTION TO VERIFY IFSC CODE ////////////////////////

   ///////////////////////// FUNCTION TO SUBMIT BENIFISHIRY TO SERVER //////////////////////////
   confirmpayee()
   {
      
   }
   ///////////////////////// END FUNCTION TO SUBMIT BENIFISHIRY TO SERVER ////////////////////////

   onChange(SelectedValue){
    console.log("Selected Value", SelectedValue);
    this.Mode =SelectedValue;
    console.log(this.Mode);
    }



  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPayeePage');
  }

}
