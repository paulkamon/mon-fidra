import { Component } from '@angular/core';
import { NavController,AlertController, App } from 'ionic-angular';
// import { AccountListPage } from '../account-list/account-list';
import { PaymentsPage } from '../payments/payments';
import { ActiveLoanListPage } from '../active-loan-list/active-loan-list';
import { ManagePayeePage } from '../manage-payee/manage-payee';
import { FdListPage } from '../fd-list/fd-list';
import { SupportPage } from '../support/support';
import { ContactPage } from '../contact/contact';
import { LegalTermsPage } from '../legal-terms/legal-terms';
import { OtherTransferPage } from '../other-transfer/other-transfer';
import { InvestmentPage } from '../investment/investment';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public app: App,public navCtrl: NavController, private alertCtrl:AlertController) {

  }
  Bankaccountlist()
  {
    this.navCtrl.push('AccountListPage');
  }
  demandes()
  {
    this.navCtrl.push('DemandesPage');
  }
  produits()
  {
    this.navCtrl.push('ProduitsPage');
  }
  factures()
  {
    this.navCtrl.push('FacturesPage');
  }

  openModal(){
    let alert = this.alertCtrl.create({
      title: 'Déconnexion',
      subTitle: 'Voulez vous vraiment vous déconnecter?',
     // buttons: ['OK']
      buttons: [ {
        cssClass: 'alertCustomCss',
        text: 'NON',
        handler: () => {
          // this.navCtrl.setRoot('RegisterPage');
          console.log('Disagree clicked');
        }
      },
      {
        cssClass: 'alertCustomCss',
        text: 'OUI',
        handler: () => {
          // this.navCtrl.setRoot('WelComePage');
          this.app.getRootNav().setRoot('WelComePage');
          console.log('Continuer clicked');
        }
      }]

    });
    alert.present();
  }
  // FundTransfer()
  // {
  //   this.navCtrl.push(PaymentsPage)
  // }
  // ActiveLoan()
  // {
  //   this.navCtrl.push(ActiveLoanListPage);
  // }

  // Beneficiary()
  // {
  //   this.navCtrl.push(ManagePayeePage);
  // }

  // GetAllFD()
  // {
  //   this.navCtrl.push(FdListPage);
  // }
  // OtherTransfer()
  // {
  //   this.navCtrl.push(OtherTransferPage);
  // }
  // Investment()
  // {
  //   this.navCtrl.push(InvestmentPage);
  // }
  // LegalTerms()
  // {
  //   this.navCtrl.push(LegalTermsPage);
  // }
  // Support()
  // {
  //   this.navCtrl.push(SupportPage);
  // }
  // Contact()
  // {
  // //  this.navCtrl.push(EnrollFingurePage);
  //   this.navCtrl.push(ContactPage);
  // }
}
