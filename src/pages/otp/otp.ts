import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the OtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
})
export class OtpPage {
  OTP;
  collection;
  char1;char2;char3;char4;;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtpPage');
  }
  first(){
    this.collection = this.char1 + this.char2 +this.char3 +this.char4;
    if(this.OTP == this.collection){
      console.log(this.collection);
   // this.navCtrl.push(FirstPage, {userEmail:this.userEmail,userMobile: this.userMobile});
    }
  }
  HomePage()
  {
    this.navCtrl.setRoot(TabsPage);
  }
  next(el) {
    el.setFocus()
  }
}
