import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App } from 'ionic-angular';

/**
 * Generated class for the AccountListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-account-list',
  templateUrl: 'account-list.html',
})
export class AccountListPage {

  constructor(public app: App,public navCtrl: NavController, public navParams: NavParams, private alertCtrl:AlertController) {
  }

  openModal(){
    let alert = this.alertCtrl.create({
      title: 'Déconnexion',
      subTitle: 'Voulez vous vraiment vous déconnecter?',
     // buttons: ['OK']
      buttons: [ {
        cssClass: 'alertCustomCss',
        text: 'NON',
        handler: () => {
          // this.navCtrl.setRoot('RegisterPage');
          console.log('Disagree clicked');
        }
      },
      {
        cssClass: 'alertCustomCss',
        text: 'OUI',
        handler: () => {
          this.app.getRootNav().setRoot('WelComePage');
          console.log('Continuer clicked');
        }
      }]

    });
    alert.present();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountListPage');
  }

  account() {
    this.navCtrl.push('InvestmentPage');
    }

  transfert() {
    this.navCtrl.push('PaymentsPage');  
  }
  // Bankaccountlist()
  // {
  //   this.navCtrl.push('AccountListPage');
  // }
  demandes()
  {
    this.navCtrl.push('DemandesPage').then(()=> {
      this.navCtrl.remove(this.navCtrl.getPrevious().index);
    });
  }
  produits()
  {
    this.navCtrl.push('ProduitsPage').then(()=> {
      this.navCtrl.remove(this.navCtrl.getPrevious().index);
    });
  }
  factures()
  {
    this.navCtrl.push('FacturesPage').then(()=> {
      this.navCtrl.remove(this.navCtrl.getPrevious().index);
    });
  }
}
